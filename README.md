# Python Flask Test

This is the URL for the official Flask Tutorial [here](https://flask.palletsprojects.com/en/1.1.x/tutorial/)


## Requirements

* python 3.6.*
* pipenv
  - pip3 install pipenv
* flask
  - pip3 install pipenv
  - FLASK_APP=flaskr FLASK_ENV=development pipenv run flask run // Runs the Application for development envs
* pytest
  -  pipenv run python -m pytest // executes test functions
* coverage
  - pipenv run coverage run -m pytest // Measures code coverage
  - pipenv run coverage report // report via terminal
  - pipenv run coverage html // report via html
